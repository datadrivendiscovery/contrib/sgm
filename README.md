### sgm

Python port of [seeded graph matching (SGM)](https://arxiv.org/pdf/1209.0367.pdf)

#### Example Usage

Simple example:
```
cd examples/synthetic
python main.py
```

More complex example:
```
cd examples/kasios/
python kasios.py --num-nodes 1000 --num-seeds 32 --backend scipy.classic.jv
```
